#define DLL_EXPORT extern "C"

#include "trade.h"
// #include <map>
// map<const char*, XTFInstrument*> instruments;

DLL_EXPORT XTFApi *WINAPI createApi()
{
    return makeXTFApi(nullptr);
}
DLL_EXPORT const char *WINAPI getVersion()
{
    return getXTFVersion();
}
DLL_EXPORT const char *WINAPI getErrorMessage(int errorCode)
{
    return getXTFErrorMessage(errorCode,  0);
}
DLL_EXPORT Spi *WINAPI createSpi()
{
    return new Spi();
}
DLL_EXPORT int WINAPI setConfig(XTFApi *api, const char *name, const char *value)
{
    return api->setConfig(name, value);
}
DLL_EXPORT int WINAPI start(XTFApi *api, XTFSpi *spi)
{
    return api->start(spi);
}
DLL_EXPORT int WINAPI stop(XTFApi *api)
{
    return api->stop();
}
DLL_EXPORT int WINAPI login(XTFApi *api)
{
    return api->login();
}
DLL_EXPORT int WINAPI logout(XTFApi *api)
{
    return api->logout();
}
DLL_EXPORT int WINAPI changePassword(XTFApi *api, const char *oldPassword, const char *newPassword)
{
    return api->changePassword(oldPassword, newPassword);
}
DLL_EXPORT int WINAPI insertOrder(XTFApi *api, XTFInstrument *inst, XTFInputOrder *inputOrder)
{
    inputOrder->instrument = inst;
    return api->insertOrder(*inputOrder);
}
// 用 local 撤单
DLL_EXPORT int WINAPI cancelOrder(XTFApi *api, long orderID)
{
    return api->cancelOrder(XTF_OIDT_Local, orderID);
}
DLL_EXPORT const WINAPI void *getInstrumentByID(XTFApi *api, const char *instrumentID)
{
    return api->getInstrumentByID(instrumentID);
}
DLL_EXPORT int WINAPI findOrders(XTFApi *api)
{
    XTFOrderFilter filter;
    return api->findOrders(filter, 0, nullptr);
}

// ----------- 行情 ----------------
DLL_EXPORT int WINAPI subscribe(XTFApi *api, const char *instrumentID)
{
    const XTFInstrument *instrument;
    instrument = api->getInstrumentByID(instrumentID);
    return api->subscribe(instrument);
}
DLL_EXPORT int WINAPI unsubscribe(XTFApi *api, const char *instrumentID)
{
    const XTFInstrument *instrument;
    instrument = api->getInstrumentByID(instrumentID);
    return api->unsubscribe(instrument);
}
DLL_EXPORT int WINAPI updateBook(XTFApi *api, const char *instrumentID, double lastPrice, double bidPrice, int bidVolume, double askPrice, int askVolume)
{
    const XTFInstrument *instrument;
    instrument = api->getInstrumentByID(instrumentID);
    return api->updateBook(instrument, lastPrice, bidPrice, bidVolume, askPrice, askVolume);
}

// 当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
DLL_EXPORT void WINAPI SetOnStart(Spi *spi, void *onFunc)
{
    spi->_OnStart = onFunc;
}
DLL_EXPORT void WINAPI SetOnStop(Spi *spi, void *onFunc)
{
    spi->_OnStop = onFunc;
}
DLL_EXPORT void WINAPI SetOnLogin(Spi *spi, void *onFunc)
{
    spi->_OnLogin = onFunc;
}
DLL_EXPORT void WINAPI SetOnLogout(Spi *spi, void *onFunc)
{
    spi->_OnLogout = onFunc;
}
DLL_EXPORT void WINAPI SetOnChangePassword(Spi *spi, void *onFunc)
{
    spi->_OnChangePassword = onFunc;
}
DLL_EXPORT void WINAPI SetOnReadyForTrading(Spi *spi, void *onFunc)
{
    spi->_OnReadyForTrading = onFunc;
}
DLL_EXPORT void WINAPI SetOnLoadFinished(Spi *spi, void *onFunc)
{
    spi->_OnLoadFinished = onFunc;
}
DLL_EXPORT void WINAPI SetOnOrder(Spi *spi, void *onFunc)
{
    spi->_OnOrder = onFunc;
}
DLL_EXPORT void WINAPI SetOnCancelOrder(Spi *spi, void *onFunc)
{
    spi->_OnCancelOrder = onFunc;
}
DLL_EXPORT void WINAPI SetOnTrade(Spi *spi, void *onFunc)
{
    spi->_OnTrade = onFunc;
}
DLL_EXPORT void WINAPI SetOnAccount(Spi *spi, void *onFunc)
{
    spi->_OnAccount = onFunc;
}
DLL_EXPORT void WINAPI SetOnExchange(Spi *spi, void *onFunc)
{
    spi->_OnExchange = onFunc;
}
DLL_EXPORT void WINAPI SetOnInstrument(Spi *spi, void *onFunc)
{
    spi->_OnInstrument = onFunc;
}
DLL_EXPORT void WINAPI SetOnBookUpdate(Spi *spi, void *onFunc)
{
    spi->_OnBookUpdate = onFunc;
}
DLL_EXPORT void WINAPI SetOnEvent(Spi *spi, void *onFunc)
{
    spi->_OnEvent = onFunc;
}
DLL_EXPORT void WINAPI SetOnError(Spi *spi, void *onFunc)
{
    spi->_OnError = onFunc;
}