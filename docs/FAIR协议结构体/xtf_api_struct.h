/**
 * @file       xtf_api_struct.h
 * @brief      柜台和客户端通信的报文结构体
 * @details    
 1. 结构体名称和字段名称完全兼容3代结构体(注意token字段扩展为4字节)
 2. 部分结构体新增部分字段(报单结构体新增InstrumentIndex, 报单时需要填写)
 3. 所有结构体都是按照64字节对齐;
 * @author     accelecom
 * @date       2022-07-20
 * @version    V4.1
 * @copyright  accelecom
 */

#ifndef __XTF_API_STRUCT_H__
#define __XTF_API_STRUCT_H__
#include <cassert>
#include <xtf_api_datatype.h>

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push,1)
///*************************以下是FAIR协议消息头和域结构体*************************///
/// 结构体说明:      FAIR格式消息头结构体(消息头);
/// 对齐说明      : 字段1字节对齐，总共14字节;
/// 变动点       : 是指和3代协议结构体的变动点;
/// 1.  Token字段类型由2字节扩展为4字节; 可以根据API接口获取;
/// 2.  结构体总共为14字节;
struct CXeleFairHeader {
  ///消息ID
  TXeleFtdcMsgIdType        MessageId;
  ///客户序号
  TXeleFtdcClientIndexType  ClientIndex;
  ///客户令牌
  TXeleFtdcTokenType        Token;
  ///消息序列号
  TXeleFtdcSequenceNoType   SeqNo;
  ///请求序号,建议单调递增, 有效范围为(0~0xfffffffe)
  TXeleFtdcReqIdType        RequestID;
};
static_assert(sizeof(CXeleFairHeader) == 14, "CXeleFairHeader size error");


/// 结构体说明:      FAIR格式 报单输入消息 域结构体(消息域)
/// 对齐说明      : 字段1字节对齐，总共50字节;
/// 变动点       : 是指和3代协议结构体的变动点;
/// 1. 新增InstrumentIndex字段, 4字节, 报单时需要填写，可以根据API接口获取;
/// 2. 结构体总共50字节;
struct CXeleFairInputOrderFieldRaw{
  ///本地报单编号
  TXeleFtdcOrderLocalNoType          OrderLocalNo;
  ///报单价格
  TXeleFtdcPriceType                 LimitPrice;  
  ///合约代码
  TXeleFtdcInstruIDType              InstrumentID;
  ///数量
  TXeleFtdcVolumeTotalOriginalType   VolumeTotalOriginal;
  ///输入报单类型
  TXeleFtdcInsertType                InsertType;
  ///最小成交数量
  TXeleFtdcMinVolumeType             MinVolume;
  ///指定前置信息(不指定前置填写为0， 指定前置需要加上偏移量10, 如指定前置3，则需要填写13)
  TXeleFtdcExchangeFrontEnumType     ExchangeFront;
  
  ///----以下为新增字段--------
  ///合约序号
  TXeleFtdcInstrumentIndexType       InstrumentIndex;
  ///预留字段
  char                               Rsv[12];
};
static_assert(sizeof(CXeleFairInputOrderFieldRaw) == 50, "CXeleFairInputOrderFieldRaw size error");


/// 结构体说明:      FAIR格式 报单操作消息域结构体(消息域)
/// 对齐说明      : 字段1字节对齐，总共50字节;
/// 变动点       : 是指和3代协议结构体的变动点;
/// 1. 新增OrderLocalNo字段, 4字节，当前版本未使用;
/// 2. 结构体总共50字节;
struct CXeleFairOrderActionFieldRaw {
  ///本地报单操作编号
  TXeleFtdcActionLocalNoType ActionLocalNo;
  ///系统报单编号
  TXeleFtdcOrderSysNoType    OrderSysNo; 
  ///报单操作标志
  TXeleFtdcActionFlagType    ActionFlag;
  
  ///----以下为新增字段--------
  ///本地报单编号
  TXeleFtdcOrderLocalNoType  OrderLocalNo;  
  ///预留字段
  char Rsv[37];
};
static_assert(sizeof(CXeleFairOrderActionFieldRaw) == 50, "CXeleFairOrderActionFieldRaw size error");


///*************************以下是FAIR协议报撤单请求完整消息结构体*************************///
/// 结构体说明:      FAIR格式 报单输入消息完整结构体(包括头和域)
/// 对齐说明      : 字段1字节对齐，结构体64字节对齐，总共64字节;
/// 变动点       : 是指和3代协议结构体的变动点;
/// 1. Token字段由2字节扩展为4字节;
/// 2. 新增InstrumentIndex字段, 4字节, 报单时需要填写;
/// 3. 结构体64字节对齐，总共50字节;
struct CXeleFairInputOrderMsg {
  ///------FAIR消息头------
  ///消息ID
  TXeleFtdcMsgIdType        MessageId;
  ///客户序号
  TXeleFtdcClientIndexType  ClientIndex;
  ///客户令牌
  TXeleFtdcTokenType        Token;
  ///消息序列号
  TXeleFtdcSequenceNoType   SeqNo;
  ///请求序号,建议单调递增, 有效范围为(0~0xfffffffe)
  TXeleFtdcReqIdType        RequestID;

  ///------FAIR消息域------
  ///本地报单编号
  TXeleFtdcOrderLocalNoType OrderLocalNo;
  ///报单价格
  TXeleFtdcPriceType        LimitPrice;  
  ///合约代码
  TXeleFtdcInstruIDType     InstrumentID;
  ///数量
  TXeleFtdcVolumeTotalOriginalType   VolumeTotalOriginal;
  ///输入报单类型(具体值类型请 参考TXeleFtdcInsertType 定义说明)
  TXeleFtdcInsertType                InsertType;
  ///最小成交数量
  TXeleFtdcMinVolumeType             MinVolume;
  ///指定前置信息(不指定前置填写为0， 指定前置需要加上偏移量10, 如指定前置3，则需要填写13)
  TXeleFtdcExchangeFrontEnumType     ExchangeFront;
  
  ///------以下为新增字段------
  ///合约序号
  TXeleFtdcInstrumentIndexType       InstrumentIndex;
  ///预留字段
  char                               Rsv[12];
};
static_assert(sizeof(CXeleFairInputOrderMsg) == 64, "CXeleFairInputOrderMsg size error");

/// 结构体说明:      FAIR格式 报单操作消息完整结构体(包括头和域)
/// 对齐说明      : 字段1字节对齐，结构体64字节对齐， 总共64字节;
/// 变动点       : 是指和3代协议结构体的变动点;
/// 1. Token字段由2字节扩展为4字节;
/// 2. 新增OrderLocalNo字段, 4字节，当前版本未使用;
/// 3. 结构体64字节对齐，总共64字节;
struct CXeleFairOrderActionMsg{
  ///------FAIR消息头------
  ///消息ID
  TXeleFtdcMsgIdType        MessageId;
  ///客户序号
  TXeleFtdcClientIndexType  ClientIndex;
  ///客户令牌
  TXeleFtdcTokenType        Token;
  ///消息序列号
  TXeleFtdcSequenceNoType   SeqNo;
  ///请求序号,建议单调递增, 有效范围为(0~0xfffffffe)
  TXeleFtdcReqIdType        RequestID;
  
  ///------FAIR消息域------
  ///本地报单操作编号
  TXeleFtdcActionLocalNoType  ActionLocalNo;
  ///系统报单编号
  TXeleFtdcOrderSysNoType     OrderSysNo; 
  ///报单操作标志
  TXeleFtdcActionFlagType     ActionFlag;
  
  ///------以下为新增字段------
  ///本地报单编号
  TXeleFtdcOrderLocalNoType   OrderLocalNo;    
  ///预留字段
  char Rsv[37];
};
static_assert(sizeof(CXeleFairOrderActionMsg) == 64, "CXeleFairOrderActionMsg size error");

///*************************以下是FAIR协议组合请求完整消息结构体*************************///
/// 结构体说明:      FAIR格式 组合报单消息完整结构体(包括头和域)
/// 对齐说明      : 字段1字节对齐，结构体64字节对齐，总共64字节;
/// 其他说明:  组合请求暂未支持
struct CXeleFairCombOrderMsg {
  ///------FAIR消息头------
  ///消息ID (0x69)
  TXeleFtdcMsgIdType        MessageId;
  ///客户序号
  TXeleFtdcClientIndexType  ClientIndex;
  ///客户令牌
  TXeleFtdcTokenType        Token;
  ///消息序列号
  TXeleFtdcSequenceNoType   SeqNo;
  ///请求序号,建议单调递增, 有效范围为(0~0xfffffffe)
  TXeleFtdcReqIdType        RequestID;

  ///------FAIR消息域------
  ///本地报单编号
  TXeleFtdcOrderLocalNoType OrderLocalNo;
  ///组合合约代码
  TXeleFtdcCombInstruIDType CombInstrumentID;
  ///组合投保标志
  TXeleFtdcCombHedgeType    CombHedgeFlag;
  ///组合状态类型(组合/解锁)
  TXeleFtdcCombActionType   CombAction; 
  ///组合数量
  TXeleFtdcCombVolumeType   Volume; 
  ///预留字段
  char                      Rsv[4];
};
static_assert(sizeof(CXeleFairCombOrderMsg) == 64, "CXeleFairCombOrderMsg size error");

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif
