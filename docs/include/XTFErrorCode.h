/**
 * @file        XTFErrorCode.h
 * @brief       错误码定义文件
 * @details     本文件定义API的常见错误码
 * @author      xinweiz
 * @date        2022-06-22
 * @version     v4.1
 * @copyright   南京艾科朗克信息科技有限公司
*/

#ifndef XTF_ERROR_CODE_H
#define XTF_ERROR_CODE_H

/// 错误码定义
const int ERRXTFAPI_NoError                       = 0;      ///< 无错误

const int ERRXTFAPI_InvalidParameter              = -10001;
const int ERRXTFAPI_NotSupported                  = -10002;
const int ERRXTFAPI_NotFound                      = -10003;
const int ERRXTFAPI_NoMemory                      = -10004;
const int ERRXTFAPI_TcpSendPartialData            = -12004;

const int ERRXTFAPI_InvalidAccount                = -11001;
const int ERRXTFAPI_InvalidPassword               = -11002;
const int ERRXTFAPI_InvalidAppID                  = -11003;
const int ERRXTFAPI_InvalidAuthCode               = -11004;
const int ERRXTFAPI_InvalidTradeIP                = -11005;
const int ERRXTFAPI_InvalidTradePort              = -11006;
const int ERRXTFAPI_InvalidQueryIP                = -11007;
const int ERRXTFAPI_InvalidQueryPort              = -11008;

const int ERRXTFAPI_InvalidImp                    = -20002;
const int ERRXTFAPI_InvalidSpi                    = -20001;
const int ERRXTFAPI_NotStarted                    = -20004;
const int ERRXTFAPI_NotLoggedIn                   = -20005;
const int ERRXTFAPI_RepeatInit                    = -20006;
const int ERRXTFAPI_RepeatStart                   = -20007;
const int ERRXTFAPI_RepeatStop                    = -20008;
const int ERRXTFAPI_RepeatLogin                   = -20009;
const int ERRXTFAPI_RepeatLogout                  = -20010;
const int ERRXTFAPI_WrongDataType                 = -20011;
const int ERRXTFAPI_RetryMaxCount                 = -20014;
const int ERRXTFAPI_AccountChanged                = -20015;

const int ERRXTFAPI_InvalidHedgeFlag              = -23011;
const int ERRXTFAPI_InvalidInstrument             = -23001;
const int ERRXTFAPI_InstrumentCreateFailed        = -23002;
const int ERRXTFAPI_InstrumentNotFound            = -23004;
const int ERRXTFAPI_InvalidProduct                = -24001;
const int ERRXTFAPI_ProductCreateFailed           = -24002;
const int ERRXTFAPI_ProductNotFound               = -24004;
const int ERRXTFAPI_ProductGroupCreateFailed      = -25002;
const int ERRXTFAPI_ProductGroupNotFound          = -25004;
const int ERRXTFAPI_InvalidExchange               = -27001;
const int ERRXTFAPI_ExchangeNotFound              = -27004;

const int ERRXTFAPI_OrderCountExceeded            = -21005;
const int ERRXTFAPI_OrderNotFound                 = -21004;
const int ERRXTFAPI_OrderStatusNotAllowedCancel   = -21006;

const int ERRXTFAPI_FunctionNotFinished           = -16001;

const int ERRXTFAPI_ClosedByUser                  = 1001;
const int ERRXTFAPI_ClosedByTimeout               = 1002;
const int ERRXTFAPI_ClosedBySendError             = 1003;
const int ERRXTFAPI_ClosedByRecvError             = 1004;
const int ERRXTFAPI_ClosedByLoginError            = 1010;
const int ERRXTFAPI_ClosedByLogoutError           = 1011;
const int ERRXTFAPI_ClosedByZipStreamError        = 1012;

#endif
